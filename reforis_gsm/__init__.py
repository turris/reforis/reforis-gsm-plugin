# Copyright (C) 2020-2024 CZ.NIC z.s.p.o. (https://www.nic.cz/)
#
# This is free software, licensed under the GNU General Public License v3.
# See /LICENSE for more information.

"""GSM plugin for reForis"""

from pathlib import Path
from http import HTTPStatus

from flask import Blueprint, current_app, jsonify, request
from flask_babel import gettext as _

from reforis.foris_controller_api.utils import validate_json, APIError

blueprint = Blueprint(
    "Gsm",
    __name__,
    url_prefix="/gsm/api",
)

BASE_DIR = Path(__file__).parent

gsm = {
    "blueprint": blueprint,
    "js_app_path": "reforis_gsm/js/app.min.js",
    "translations_path": BASE_DIR / "translations",
}


@blueprint.route("/settings", methods=["GET"])
def get_settings():
    """Get GSM settings"""
    return jsonify(current_app.backend.perform("lte", "get_settings"))


@blueprint.route("/settings", methods=["POST"])
def post_settings():
    """Update GSM settings"""
    validate_json(request.json, {"devices": list})

    response = current_app.backend.perform("lte", "update_settings", request.json)

    if response.get("result") is not True:
        if response.get("error_code") == "incorrect-pin":
            raise APIError(
                _("Incorrect PIN code! Note: Your SIM card will be blocked after 3 unsuccessful attempts."),
                HTTPStatus.BAD_REQUEST,
            )
        raise APIError(_("Cannot update GSM settings"), HTTPStatus.INTERNAL_SERVER_ERROR)

    return jsonify(response), HTTPStatus.CREATED
