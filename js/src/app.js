/*
 * Copyright (C) 2020-2024 CZ.NIC z.s.p.o. (https://www.nic.cz/)
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See /LICENSE for more information.
 */

import GSM from "./GSM/GSM";

const GSMPlugin = {
    name: _("GSM"),
    weight: 1,
    submenuId: "network-settings",
    path: "/gsm",
    component: GSM,
};

ForisPlugins.push(GSMPlugin);
