/*
 * Copyright (C) 2020-2024 CZ.NIC z.s.p.o. (https://www.nic.cz/)
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See /LICENSE for more information.
 */

const settings = {
    devices: [
        {
            apn: "internet",
            auth: {
                type: "none",
            },
            id: "wwan0",
            info: {
                manufacturer: "Quectel",
                model: "EP06-E",
                operator: "O2",
                registration: "home",
                sim_lock: true,
                state: "connected",
            },
            pin: "1234",
            qmi_device: "/dev/cdc-wdm0",
        },
    ],
};

export const withoutPin = {
    devices: [
        {
            apn: "internet",
            auth: {
                type: "none",
            },
            id: "wwan0",
            info: {
                manufacturer: "Quectel",
                model: "EP06-E",
                operator: "O2",
                registration: "home",
                sim_lock: true,
                state: "locked",
            },
            pin: "unset",
            qmi_device: "/dev/cdc-wdm0",
        },
    ],
};

export default settings;
