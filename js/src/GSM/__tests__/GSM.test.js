/*
 * Copyright (C) 2020-2024 CZ.NIC z.s.p.o. (https://www.nic.cz/)
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See /LICENSE for more information.
 */

import React from "react";
import mockAxios from "jest-mock-axios";
import {
    render,
    wait,
    waitForElement,
    getByText,
    fireEvent,
    getByLabelText,
} from "foris/testUtils/customTestRender";
import { mockJSONError } from "foris/testUtils/network";
import { WebSockets } from "foris";
import { mockSetAlert } from "foris/testUtils/alertContextMock";

import GSM from "../GSM";
import settings, { withoutPin } from "./__fixtures__/settings";

describe("<GSM />", () => {
    let container;
    let webSockets;

    beforeEach(() => {
        webSockets = new WebSockets();
        ({ container } = render(<GSM ws={webSockets} />));
    });

    it("should render spinner", () => {
        expect(container).toMatchSnapshot();
    });

    it("should render component", async () => {
        expect(mockAxios.get).toBeCalledWith(
            "/reforis/gsm/api/settings",
            expect.anything()
        );
        mockAxios.mockResponse({ data: settings });
        expect(mockAxios.get).toBeCalledWith(
            "/reforis/gsm/api/settings",
            expect.anything()
        );
        mockAxios.mockResponse({ data: settings });
        await waitForElement(() =>
            getByText(container, settings.devices[0].info.operator)
        );
        expect(container).toMatchSnapshot();
    });

    it("should handle GET error", async () => {
        mockJSONError();
        await wait(() =>
            expect(
                getByText(container, "An error occurred while fetching data.")
            ).toBeTruthy()
        );
    });

    it("should handle POST request", async () => {
        expect(mockAxios.get).toBeCalledWith(
            "/reforis/gsm/api/settings",
            expect.anything()
        );
        mockAxios.mockResponse({ data: settings });
        expect(mockAxios.get).toBeCalledWith(
            "/reforis/gsm/api/settings",
            expect.anything()
        );
        mockAxios.mockResponse({ data: settings });
        await waitForElement(() =>
            getByText(container, settings.devices[0].info.operator)
        );

        fireEvent.change(getByLabelText(container, "APN"), {
            target: { value: "apn apn" },
        });

        fireEvent.click(getByText(container, "Save").parentElement);

        expect(mockAxios.post).toBeCalledWith(
            "/reforis/gsm/api/settings",
            {
                devices: [
                    {
                        apn: "apn apn",
                        auth: {
                            type: "none",
                        },
                        id: "wwan0",
                        pin: "1234",
                        qmi_device: "/dev/cdc-wdm0",
                    },
                ],
            },
            expect.anything()
        );
        mockAxios.mockResponse({ data: { result: true } });

        await wait(() =>
            expect(mockAxios.get).toBeCalledWith(
                "/reforis/gsm/api/settings",
                expect.anything()
            )
        );
        mockAxios.mockResponse({
            data: {
                ...settings,
                devices: [{ ...settings.devices[0], apn: "apn apn" }],
            },
        });
        await waitForElement(() => getByText(container, "O2"));
        expect(getByLabelText(container, "APN").value).toBe("apn apn");
        expect(container).toMatchSnapshot();
    });

    it("should show invalid PIN error", async () => {
        expect(mockAxios.get).toBeCalledWith(
            "/reforis/gsm/api/settings",
            expect.anything()
        );
        mockAxios.mockResponse({ data: withoutPin });
        expect(mockAxios.get).toBeCalledWith(
            "/reforis/gsm/api/settings",
            expect.anything()
        );
        mockAxios.mockResponse({ data: withoutPin });
        await waitForElement(() =>
            getByText(container, settings.devices[0].info.operator)
        );

        fireEvent.change(getByLabelText(container, "PIN"), {
            target: { value: "123" },
        });

        fireEvent.click(getByText(container, "Save").parentElement);

        await wait(() =>
            expect(getByText(container, "PIN must be a 4-digit number."))
        );
    });

    it("should show required PIN error", async () => {
        expect(mockAxios.get).toBeCalledWith(
            "/reforis/gsm/api/settings",
            expect.anything()
        );
        mockAxios.mockResponse({ data: withoutPin });
        expect(mockAxios.get).toBeCalledWith(
            "/reforis/gsm/api/settings",
            expect.anything()
        );
        mockAxios.mockResponse({ data: withoutPin });
        await waitForElement(() =>
            getByText(container, settings.devices[0].info.operator)
        );

        fireEvent.change(getByLabelText(container, "PIN"), {
            target: { value: "" },
        });

        fireEvent.click(getByText(container, "Save").parentElement);

        await wait(() => expect(getByText(container, "Please enter a PIN.")));
    });

    it("should handle submitting a wrong PIN", async () => {
        expect(mockAxios.get).toBeCalledWith(
            "/reforis/gsm/api/settings",
            expect.anything()
        );
        mockAxios.mockResponse({ data: withoutPin });
        expect(mockAxios.get).toBeCalledWith(
            "/reforis/gsm/api/settings",
            expect.anything()
        );
        mockAxios.mockResponse({ data: withoutPin });
        await waitForElement(() =>
            getByText(container, settings.devices[0].info.operator)
        );

        fireEvent.change(getByLabelText(container, "PIN"), {
            target: { value: "1234" },
        });

        fireEvent.click(getByText(container, "Save").parentElement);

        expect(mockAxios.post).toBeCalledWith(
            "/reforis/gsm/api/settings",
            {
                devices: [
                    {
                        apn: "internet",
                        auth: {
                            type: "none",
                        },
                        id: "wwan0",
                        pin: "1234",
                        qmi_device: "/dev/cdc-wdm0",
                    },
                ],
            },
            expect.anything()
        );
        mockJSONError(
            "Incorrect PIN code! Note: Your SIM card will be blocked after 3 unsuccessful attempts."
        );
        await wait(() => {
            expect(mockSetAlert).toBeCalledWith(
                "Incorrect PIN code! Note: Your SIM card will be blocked after 3 unsuccessful attempts."
            );
        });
    });
});
