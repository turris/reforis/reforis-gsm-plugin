/*
 * Copyright (C) 2020-2024 CZ.NIC z.s.p.o. (https://www.nic.cz/)
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See /LICENSE for more information.
 */

import React from "react";
import { render } from "foris/testUtils/customTestRender";

import ConfigurationForm from "../ConfigurationForm";
import formData from "./__fixtures__/settings";

describe("<ConfigurationForm />", () => {
    it("should render component", () => {
        const setFormValue = jest.fn((updater) => {
            return (event) => {
                // Mock implementation to call the updater with a dummy value
                updater(event.target.value);
            };
        });

        const { container } = render(
            <ConfigurationForm
                formData={formData.devices[0]}
                formErrors={{}}
                setFormValue={setFormValue}
            />
        );
        expect(container).toMatchSnapshot();
    });
});
