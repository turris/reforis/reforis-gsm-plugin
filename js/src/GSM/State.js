/*
 * Copyright (C) 2020-2024 CZ.NIC z.s.p.o. (https://www.nic.cz/)
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See /LICENSE for more information.
 */

import React from "react";

import { withSpinnerOnSending, withErrorMessage, formFieldsSize } from "foris";
import PropTypes from "prop-types";
import { createPortal } from "react-dom";

import { stateToColor, GSM_STATES, REGISTRATION_STATES } from "./states";

State.propTypes = {
    gsmState: PropTypes.object.isRequired,
};

State.defaultProps = {
    gsmState: {
        state: "loading",
        data: {
            devices: [
                {
                    info: {
                        state: "unknown",
                        sim_lock: false,
                        manufacturer: "",
                        model: "",
                        operator: "",
                        registration: "",
                    },
                },
            ],
        },
    },
};

export default function State({ gsmState }) {
    const stateContainer = document.getElementById("gsm-state");

    return createPortal(
        <div className={formFieldsSize}>
            <h2>{_("GSM State")}</h2>
            <StateWithErrorAndSpinner
                apiState={gsmState.state}
                data={gsmState.data}
            />
        </div>,
        stateContainer
    );
}

StateCard.propTypes = {
    data: PropTypes.shape({
        devices: PropTypes.arrayOf(
            PropTypes.shape({
                info: PropTypes.shape({
                    state: PropTypes.string.isRequired,
                    sim_lock: PropTypes.bool.isRequired,
                    manufacturer: PropTypes.string.isRequired,
                    model: PropTypes.string.isRequired,
                    operator: PropTypes.string.isRequired,
                    registration: PropTypes.string.isRequired,
                }).isRequired,
            })
        ).isRequired,
    }).isRequired,
};

function StateCard({ data: { devices } }) {
    const {
        state,
        sim_lock: isSIMLocked,
        manufacturer,
        model,
        operator,
        registration,
    } = devices[0].info;
    return (
        <div className="row no-gutters justify-content-center">
            <div className="col-md-6 d-flex flex-column align-items-center justify-content-center">
                <i
                    className={`fas fa-sim-card fa-4x mb-2 ${stateToColor[state]}`}
                />
                <h5>{operator}</h5>
            </div>
            <div className="col-md-6">
                <div className="card-body">
                    <div className="table-responsive">
                        <table className="table table-borderless table-hover">
                            <tbody>
                                <tr>
                                    <th scope="row">{_("State")}</th>
                                    <td className={stateToColor[state]}>
                                        <b>{GSM_STATES[state]}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">{_("PIN-locked")}</th>
                                    <td>
                                        {isSIMLocked ? (
                                            <i
                                                className="fas fa-lock"
                                                title={_("Locked")}
                                            />
                                        ) : (
                                            <i
                                                className="fas fa-unlock"
                                                title={_("Unlocked")}
                                            />
                                        )}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        {_("Registration status")}
                                    </th>
                                    <td>{`${REGISTRATION_STATES[registration]}`}</td>
                                </tr>
                                <tr>
                                    <th scope="row">{_("Modem device")}</th>
                                    <td>{`${manufacturer} ${model}`}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
}

const StateWithErrorAndSpinner = withSpinnerOnSending(
    withErrorMessage(StateCard)
);
