/*
 * Copyright (C) 2020-2024 CZ.NIC z.s.p.o. (https://www.nic.cz/)
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See /LICENSE for more information.
 */

import React, { useEffect } from "react";

import {
    useAPIGet,
    ForisForm,
    undefinedIfEmpty,
    withoutUndefinedKeys,
} from "foris";
import PropTypes from "prop-types";

import API_URLs from "API";

import ConfigurationForm from "./ConfigurationForm";
import State from "./State";

GSM.propTypes = {
    ws: PropTypes.object.isRequired,
};

export default function GSM({ ws }) {
    const [settings, getSettings] = useAPIGet(API_URLs.settings);

    useEffect(() => {
        getSettings();
    }, [getSettings]);

    return (
        <>
            <h1>GSM</h1>
            <p>
                {_(
                    "Here you can seamlessly configure your GSM module, ensuring reliable and secure connectivity for your network needs."
                )}
            </p>
            <div id="gsm-state" />
            <ForisForm
                ws={ws}
                forisConfig={{
                    endpoint: API_URLs.settings,
                    wsModule: "gsm",
                }}
                validator={validator}
                postCallback={getSettings}
                prepData={prepData}
                prepDataToSubmit={prepDataToSubmit}
            >
                <State gsmState={settings} />
                <ConfigurationForm />
            </ForisForm>
        </>
    );
}

function isValidPin(pin) {
    return /^\d{4}$/.test(pin);
}

function validator(formData) {
    if (!formData || formData.length === 0) {
        return undefined;
    }

    const errors = {};

    if (formData.info.sim_lock) {
        if (formData.pin === "unset" || formData.pin === "") {
            errors.pin = _("Please enter a PIN.");
        } else if (!isValidPin(formData.pin)) {
            errors.pin = _("PIN must be a 4-digit number.");
        }
    }

    if (formData.auth.type !== "none") {
        if (formData.auth.username === "") {
            errors.username = _("Please enter a username.");
        }

        if (formData.auth.password === "") {
            errors.password = _("Please enter a password.");
        }
    }

    return undefinedIfEmpty(withoutUndefinedKeys(errors));
}

function prepData(formData) {
    return formData.devices[0];
}

function prepDataToSubmit(data) {
    delete data.info;

    if (data.pin === "unset") {
        delete data.pin;
    }

    if (data.auth.type === "none") {
        delete data.auth.username;
        delete data.auth.password;
    }

    return { devices: [data] };
}
