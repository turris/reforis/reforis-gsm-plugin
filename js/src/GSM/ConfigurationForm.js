/*
 * Copyright (C) 2020-2024 CZ.NIC z.s.p.o. (https://www.nic.cz/)
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See /LICENSE for more information.
 */

import React from "react";

import { TextInput, PasswordInput, Select } from "foris";
import PropTypes from "prop-types";

ConfigurationForm.propTypes = {
    formData: PropTypes.shape({
        apn: PropTypes.string,
        pin: PropTypes.string,
        auth: PropTypes.shape({
            type: PropTypes.string,
            username: PropTypes.string,
            password: PropTypes.string,
        }),
        info: PropTypes.shape({
            sim_lock: PropTypes.bool,
            state: PropTypes.string,
        }),
    }),
    setFormValue: PropTypes.func.isRequired,
    formErrors: PropTypes.object,
};

ConfigurationForm.defaultProps = {
    formData: {},
    formErrors: {},
    setFormValue: () => {},
};

const AUTH_TYPES = {
    none: _("None"),
    pap: _("PAP"),
    chap: _("CHAP"),
    both: _("PAP/CHAP (both)"),
};

export default function ConfigurationForm({
    formData,
    formErrors,
    setFormValue,
}) {
    return (
        <>
            <h2>{_("GSM Settings")}</h2>
            <TextInput
                label={_("APN")}
                value={formData.apn || ""}
                placeholder={_("Enter APN")}
                helpText={_(
                    "The Access Point Name (APN) provided by your mobile carrier."
                )}
                onChange={setFormValue((value) => ({
                    apn: { $set: value },
                }))}
            />
            {formData.info.sim_lock && (
                <PasswordInput
                    label={_("PIN")}
                    placeholder={_("Enter PIN")}
                    value={formData.pin !== "unset" ? formData.pin : ""}
                    onChange={setFormValue((value) => ({
                        pin: { $set: value },
                    }))}
                    error={(formErrors || {}).pin || null}
                    helpText={_(
                        "The Personal Identification Number (PIN) is a numeric password used for your SIM card."
                    )}
                    disabled={
                        formData.info.state !== "locked" &&
                        formData.pin !== "unset"
                    }
                    withEye
                    maxLength={4}
                    inputMode="numeric"
                />
            )}
            <Select
                label={_("Authentication type")}
                value={formData.auth.type}
                choices={AUTH_TYPES}
                helpText={_(
                    "The security method used by your carrier to authenticate your connection."
                )}
                onChange={setFormValue((value) => {
                    return {
                        auth: {
                            type: { $set: value },
                            $merge: {
                                username: formData.auth.username || "",
                                password: formData.auth.password || "",
                            },
                        },
                    };
                })}
            />
            {formData.auth.type !== "none" && (
                <>
                    <TextInput
                        label={_("Username")}
                        value={formData.auth.username || ""}
                        error={(formErrors || {}).username || null}
                        onChange={setFormValue((value) => ({
                            auth: {
                                username: { $set: value },
                            },
                        }))}
                    />
                    <PasswordInput
                        label={_("Password")}
                        value={formData.auth.password || ""}
                        error={(formErrors || {}).password || null}
                        onChange={setFormValue((value) => ({
                            auth: {
                                password: { $set: value },
                            },
                        }))}
                        withEye
                    />
                </>
            )}
        </>
    );
}
