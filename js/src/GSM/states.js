/*
 * Copyright (C) 2020-2024 CZ.NIC z.s.p.o. (https://www.nic.cz/)
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See /LICENSE for more information.
 */

export const GSM_STATES = {
    locked: _("Locked"),
    connected: _("Connected"),
    disabled: _("Disabled"),
    registered: _("Registered"),
    initializing: _("Initializing"),
};

export const REGISTRATION_STATES = {
    home: _("Home network"),
    roaming: _("Roaming"),
    idle: _("Idle"),
    "--": _("Unknown"),
};

export const stateToColor = {
    locked: "text-warning",
    connected: "text-success",
    disabled: "text-danger",
    registered: "text-info",
    initializing: "text-primary",
};
