# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2024-06-13

### Added

-   Added & updated Weblate translations
-   Added null check in GSM validator function

### Changed

-   Updated Foris JS library to v6.0.0
-   Updated dependencies in package.json
-   Updated Makefile to install FC LTE module
-   NPM audit fix

## [0.1.0] - 2024-05-29

-   Initial version
-   Added LTE configuration

[unreleased]: https://gitlab.nic.cz/turris/reforis/reforis-gsm/-/compare/v1.0.0...master
[1.0.0]: https://gitlab.nic.cz/turris/reforis/reforis-gsm/-/compare/v0.1.0...v1.0.0
[0.1.0]: https://gitlab.nic.cz/turris/reforis/reforis-gsm/-/tags/v0.1.0
