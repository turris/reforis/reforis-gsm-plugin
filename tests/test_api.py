# Copyright (C) 2020-2024 CZ.NIC z.s.p.o. (https://www.nic.cz/)
#
# This is free software, licensed under the GNU General Public License v3.
# See /LICENSE for more information.

from http import HTTPStatus
from reforis.test_utils import mock_backend_response


@mock_backend_response({"lte": {"get_settings": ["foo", "bar"]}})
def test_get_settings(client):
    response = client.get("/gsm/api/settings")
    assert response.status_code == HTTPStatus.OK
    assert response.json == ["foo", "bar"]


@mock_backend_response({"lte": {"update_settings": {"result": True}}})
def test_post_settings_invalid_json(client):
    response = client.post("/gsm/api/settings", json=False)
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert response.json == "Invalid JSON"


@mock_backend_response({"lte": {"update_settings": {"key": "value"}}})
def test_post_settings_backend_error(client):
    response = client.post("/gsm/api/settings", json={"devices": []})
    assert response.status_code == HTTPStatus.INTERNAL_SERVER_ERROR
    assert response.json == "Cannot update GSM settings"
